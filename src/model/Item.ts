export interface Item {
  itemId: number;
  itemName: string;
  itemCode: string;
  stock: number;
  price: number;
  isAvailable: boolean;
  lastRestock: Date;
  itemPic: string;
}
