import { Customer } from './Customer';
import { Item } from './Item';

export interface Order {
  orderId: number;
  orderCode: string;
  orderDate: Date;
  totalPrice: number;
  quantity: number;
  customer: Customer;
  item: Item;
}
