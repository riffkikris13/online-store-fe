import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Item } from '../model/Item';

const url = 'http://localhost:8000/v1/api/item';

interface ItemFetch {
  items: Item[];
  totalPages: number;
}

@Injectable({
  providedIn: 'root',
})
export class ItemrService {
  constructor(private http: HttpClient) {}

  getItem(id: number): Observable<Item> {
    return this.http.get<Item>(url + '/detail/' + id);
  }

  getItems(name: string, page: number, size: number): Observable<ItemFetch> {
    const params = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('name', name);
    return this.http.get<ItemFetch>(url, { params });
  }

  createItem(data: any): Observable<any> {
    return this.http.post(url, data);
  }

  updateItem(id: number, data: any): Observable<any> {
    return this.http.patch(url + '/' + id, data);
  }

  deleteItem(id: number): Observable<any> {
    return this.http.delete(url + '/' + id);
  }

  deleteItemAndRefresh(
    id: number,
    page: number,
    size: number,
    name: string
  ): Observable<ItemFetch> {
    return this.deleteItem(id).pipe(
      switchMap(() => this.getItems(name, page, size))
    );
  }
}
