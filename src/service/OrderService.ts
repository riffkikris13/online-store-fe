import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Order } from '../model/Order';

const url = 'http://localhost:8000/v1/api/order';

interface OrderFetch {
  orders: Order[];
  totalPages: number;
}

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(private http: HttpClient) {}

  getOrder(id: number): Observable<Order> {
    return this.http.get<Order>(url + '/detail/' + id);
  }

  getOrders(
    startDate: string,
    endDate: string,
    page: number,
    size: number
  ): Observable<OrderFetch> {
    const params = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('startDate', startDate)
      .set('endDate', endDate);
    return this.http.get<OrderFetch>(url, { params });
  }

  createOrder(data: any): Observable<any> {
    return this.http.post(url, data);
  }

  updateOrder(id: number, data: any): Observable<any> {
    return this.http.patch(url + '/' + id, data);
  }

  deleteOrder(id: number): Observable<any> {
    return this.http.delete(url + '/' + id);
  }

  deleteOrderAndRefresh(
    id: number,
    page: number,
    size: number,
    startDate: string,
    endDate: string
  ): Observable<OrderFetch> {
    return this.deleteOrder(id).pipe(
      switchMap(() => this.getOrders(startDate, endDate, page, size))
    );
  }
}
