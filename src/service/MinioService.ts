import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk';

@Injectable({
  providedIn: 'root',
})
export class MinioService {
  private s3: AWS.S3;

  constructor() {
    AWS.config.update({
      accessKeyId: 'root',
      secretAccessKey: '12345678',
    });

    this.s3 = new AWS.S3({
      endpoint: 'http://localhost:9000',
      s3ForcePathStyle: true,
    });
  }

  getObject(bucket: string, key: string) {
    const params = {
      Bucket: bucket,
      Key: key,
      ServiceName: 's3',
    };

    return this.s3.getObject(params).promise();
  }
}
