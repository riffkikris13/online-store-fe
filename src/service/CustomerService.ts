import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer } from '../model/Customer';
import { switchMap } from 'rxjs/operators';

const url = 'http://localhost:8000/v1/api/customer';

interface CustomerFetch {
  customers: Customer[];
  totalPages: number;
}

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  constructor(private http: HttpClient) {}

  getCustomer(id: number): Observable<Customer> {
    return this.http.get<Customer>(url + '/detail/' + id);
  }

  getCustomers(
    name: string,
    page: number,
    size: number
  ): Observable<CustomerFetch> {
    const params = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('name', name);
    return this.http.get<CustomerFetch>(url, { params });
  }

  createCustomer(data: any): Observable<any> {
    return this.http.post(url, data);
  }

  updateCustomer(id: number, data: any): Observable<any> {
    return this.http.patch(url + '/' + id, data);
  }

  deleteCustomer(id: number): Observable<any> {
    return this.http.delete(url + '/' + id);
  }

  deleteCustomerAndRefresh(
    id: number,
    page: number,
    size: number,
    name: string
  ): Observable<CustomerFetch> {
    return this.deleteCustomer(id).pipe(
      switchMap(() => this.getCustomers(name, page, size))
    );
  }
}
