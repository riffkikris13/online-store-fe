import {
  Component,
  ViewEncapsulation,
  HostListener,
  OnInit,
} from '@angular/core';
import { Router, NavigationStart, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  title = 'online-store-fe';
  currentContent: string = '';
  sideBarStatus: boolean = true;

  constructor(private router: Router) {}

  receiveContent($event: any): void {
    this.currentContent = $event;
  }

  receiveSB(): void {
    this.sideBarStatus = !this.sideBarStatus;
  }

  receiveSBP(e: boolean): void {
    this.sideBarStatus = e;
  }

  @HostListener('window:resize')
  onResize() {
    this.checkScreenSize();
  }

  ngOnInit() {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        const currentUrl = event.url;
        this.currentContent = currentUrl.split('/')[1];
      }
    });
  }

  checkScreenSize() {
    this.sideBarStatus = window.innerWidth >= 992;
  }
}
