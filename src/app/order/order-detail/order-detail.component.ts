import { Component } from '@angular/core';
import { Order } from '../../../model/Order';
import { OrderService } from '../../../service/OrderService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'order-detail',
  templateUrl: './order-detail.component.html',
  styleUrl: './order-detail.component.scss',
})
export class OrderDetailComponent {
  order: Order | null = null;

  constructor(
    private orderService: OrderService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((data) => {
      this.orderService
        .getOrder(Number(data.get('id')))
        .subscribe((response) => {
          this.order = response;
        });
    });
  }
}
