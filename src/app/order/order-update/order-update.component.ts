import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Order } from '../../../model/Order';
import { OrderService } from '../../../service/OrderService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'order-update',
  templateUrl: './order-update.component.html',
  styleUrl: './order-update.component.scss',
})
export class OrderUpdateComponent implements OnInit {
  form: FormGroup;
  pict: File | undefined;
  order: Order | null = null;

  constructor(
    fb: FormBuilder,
    private orderService: OrderService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.form = fb.group({
      quantity: ['', Validators.required],
      'total-price': [{ value: '', disabled: true }, Validators.required],
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((data) => {
      this.orderService
        .getOrder(Number(data.get('id')))
        .subscribe((response) => {
          this.order = response;
          this.setFormValues(response);
        });
    });
  }

  setFormValues(order: Order): void {
    this.form.patchValue({
      quantity: order.quantity,
      'total-price': order.totalPrice,
    });
  }

  onQuantityInput(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    const value = parseFloat(inputElement.value);
    const price = Number(this.order?.item.price);
    console.log(price);

    if (!isNaN(value) && !isNaN(price)) {
      this.form.patchValue({
        'total-price': price * value,
      });
    } else {
      this.form.patchValue({
        'total-price': 0,
      });
    }
  }

  onSubmit(): void {
    this.route.paramMap.subscribe((data) => {
      this.orderService
        .updateOrder(Number(data.get('id')), {
          quantity: this.form.get('quantity')?.value,
          totalPrice: this.form.get('total-price')?.value,
        })
        .subscribe(() => {
          this.router.navigate(['/order']);
        });
    });
  }
}
