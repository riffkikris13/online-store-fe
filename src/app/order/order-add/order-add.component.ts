import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderService } from '../../../service/OrderService';
import { Router } from '@angular/router';
import { Observable, map, of, startWith, switchMap } from 'rxjs';
import { CustomerService } from '../../../service/CustomerService';
import { Customer } from '../../../model/Customer';
import { ItemrService } from '../../../service/ItemService';
import { Item } from '../../../model/Item';

@Component({
  selector: 'order-add',
  templateUrl: './order-add.component.html',
  styleUrl: './order-add.component.scss',
})
export class OrderAddComponent {
  form: FormGroup;
  filteredCustomers: Observable<Customer[]> | undefined;
  customer: Customer | undefined;
  filteredItems: Observable<Item[]> | undefined;
  item: Item | undefined;

  constructor(
    fb: FormBuilder,
    private customerService: CustomerService,
    private itemService: ItemrService,
    private orderService: OrderService,
    private router: Router
  ) {
    this.form = fb.group({
      customer: ['', Validators.required],
      item: ['', Validators.required],
      quantity: ['', [Validators.required]],
      'total-price': [{ value: '', disabled: true }, Validators.required],
    });
  }

  onInputClickCustomer() {
    this.form
      .get('customer')!
      .valueChanges.pipe(
        switchMap((value) => {
          return this.customerService
            .getCustomers(value, 0, 10)
            .pipe(map((response) => response.customers));
        })
      )
      .subscribe((options) => (this.filteredCustomers = of(options)));
  }

  onOptionSelectedCustomer(event: any) {
    const selectedOption = event.option.value;
    this.filteredCustomers?.subscribe((options) => {
      this.customer = options.find(
        (option) => option.customerName === selectedOption
      );
      console.log('Selected customer:', this.customer);
    });
  }

  onInputClickItem() {
    this.form
      .get('item')!
      .valueChanges.pipe(
        switchMap((value) => {
          return this.itemService
            .getItems(value, 0, 10)
            .pipe(map((response) => response.items));
        })
      )
      .subscribe((options) => (this.filteredItems = of(options)));
  }

  onOptionSelectedItem(event: any) {
    const selectedOption = event.option.value;
    this.filteredItems?.subscribe((options) => {
      this.item = options.find((option) => option.itemName === selectedOption);
      console.log('Selected item:', this.item);
      if (this.item) {
        const maxQuantity = this.item.stock;
        this.form
          .get('quantity')!
          .setValidators([
            Validators.required,
            Validators.min(1),
            Validators.max(maxQuantity),
          ]);
        this.form.get('quantity')!.updateValueAndValidity();
      }
    });
  }

  onQuantityInput(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    const value = parseFloat(inputElement.value);
    const price = this.item?.price;
    if (!isNaN(value) && price) {
      this.form.patchValue({
        'total-price': price * value,
      });
    } else {
      this.form.patchValue({
        'total-price': 0,
      });
    }
  }

  onSubmit(): void {
    this.orderService
      .createOrder({
        customerId: this.customer?.customerId,
        itemId: this.item?.itemId,
        quantity: this.form.get('quantity')?.value,
        totalPrice: this.form.get('total-price')?.value,
      })
      .subscribe(() => {
        this.router.navigate(['/order']);
      });
  }
}
