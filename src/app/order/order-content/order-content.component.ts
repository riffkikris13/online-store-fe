import { Component, ViewChild } from '@angular/core';
import { Order } from '../../../model/Order';
import { MatPaginator } from '@angular/material/paginator';
import { OrderService } from '../../../service/OrderService';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'order-content',
  templateUrl: './order-content.component.html',
  styleUrl: './order-content.component.scss',
})
export class OrderContentComponent {
  orders: Order[] = [];
  totalData: number = 0;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  startDate: string = '';
  endDate: string = '';

  range = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });

  constructor(private orderService: OrderService) {}

  ngAfterViewInit(): void {
    this.orderService
      .getOrders(
        this.startDate,
        this.endDate,
        this.paginator.pageIndex,
        this.paginator.pageSize
      )
      .subscribe((data) => {
        this.orders = data.orders;
        this.totalData = data.totalPages;
      });
  }

  startDateChanged(event: any) {
    this.startDate = this.formatDate(event.value);
    this.orderService
      .getOrders(
        this.startDate,
        this.endDate,
        this.paginator.pageIndex,
        this.paginator.pageSize
      )
      .subscribe((data) => {
        this.orders = data.orders;
        this.totalData = data.totalPages;
      });
  }

  endDateChanged(event: any) {
    this.endDate = this.formatDate(event.value);
    this.orderService
      .getOrders(
        this.startDate,
        this.endDate,
        this.paginator.pageIndex,
        this.paginator.pageSize
      )
      .subscribe((data) => {
        this.orders = data.orders;
        this.totalData = data.totalPages;
      });
  }

  formatDate(date: Date): string {
    const year = date.getFullYear();
    const month = this.padZero(date.getMonth() + 1);
    const day = this.padZero(date.getDate());
    const hours = this.padZero(date.getHours());
    const minutes = this.padZero(date.getMinutes());
    const seconds = this.padZero(date.getSeconds());
    return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}`;
  }

  padZero(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }

  onPageChange() {
    this.orderService
      .getOrders(
        this.startDate,
        this.endDate,
        this.paginator.pageIndex,
        this.paginator.pageSize
      )
      .subscribe((data) => {
        this.orders = data.orders;
        this.totalData = data.totalPages;
      });
  }

  deleteOrder(id: number): void {
    this.orderService
      .deleteOrderAndRefresh(
        id,
        this.paginator.pageIndex,
        this.paginator.pageSize,
        this.startDate,
        this.endDate
      )
      .subscribe((data) => {
        this.orders = data.orders;
        this.totalData = data.totalPages;
      });
  }
}
