import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { ItemrService } from '../../../service/ItemService';
import { Item } from '../../../model/Item';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'item-content',
  templateUrl: './item-content.component.html',
  styleUrl: './item-content.component.scss',
})
export class ItemContentComponent implements AfterViewInit {
  items: Item[] = [];
  totalData: number = 0;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  name: string = '';

  constructor(private itemService: ItemrService) {}

  ngAfterViewInit(): void {
    this.itemService
      .getItems(this.name, this.paginator.pageIndex, this.paginator.pageSize)
      .subscribe((data) => {
        console.log(data);

        this.items = data.items;
        this.totalData = data.totalPages;
      });
  }

  onPageChange() {
    this.itemService
      .getItems(this.name, this.paginator.pageIndex, this.paginator.pageSize)
      .subscribe((data) => {
        this.items = data.items;
        this.totalData = data.totalPages;
      });
  }

  onSubmit(): void {
    this.itemService
      .getItems(this.name, this.paginator.pageIndex, this.paginator.pageSize)
      .subscribe((data) => {
        this.items = data.items;
        this.totalData = data.totalPages;
      });
    this.name = '';
  }

  deleteItem(id: number): void {
    this.itemService
      .deleteItemAndRefresh(
        id,
        this.paginator.pageIndex,
        this.paginator.pageSize,
        this.name
      )
      .subscribe((data) => {
        this.items = data.items;
        this.totalData = data.totalPages;
      });
  }
}
