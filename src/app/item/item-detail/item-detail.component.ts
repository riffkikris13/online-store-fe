import { Component } from '@angular/core';
import { Item } from '../../../model/Item';
import { ItemrService } from '../../../service/ItemService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'item-detail',
  templateUrl: './item-detail.component.html',
  styleUrl: './item-detail.component.scss',
})
export class ItemDetailComponent {
  item: Item | null = null;

  constructor(
    private itemService: ItemrService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((data) => {
      this.itemService.getItem(Number(data.get('id'))).subscribe((response) => {
        this.item = response;
      });
    });
  }
}
