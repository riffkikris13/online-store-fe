import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Item } from '../../../model/Item';
import { ItemrService } from '../../../service/ItemService';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-item-update',
  templateUrl: './item-update.component.html',
  styleUrl: './item-update.component.scss',
})
export class ItemUpdateComponent implements OnInit {
  form: FormGroup;
  pict: File | undefined;
  item: Item | null = null;

  constructor(
    fb: FormBuilder,
    private itemService: ItemrService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.form = fb.group({
      name: ['', Validators.required],
      stock: ['', Validators.required],
      price: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((data) => {
      this.itemService.getItem(Number(data.get('id'))).subscribe((response) => {
        this.item = response;
        this.setFormValues(response);
      });
    });
  }

  setFormValues(item: Item): void {
    this.form.patchValue({
      name: item.itemName,
      price: item.price,
      stock: item.stock,
    });
  }

  onFileSelected(event: any) {
    this.pict = event.target.files[0];
  }

  onSubmit(): void {
    const formData = new FormData();
    formData.append('itemName', this.form.get('name')?.value);
    formData.append('price', this.form.get('price')?.value);
    formData.append('stock', this.form.get('stock')?.value);
    if (this.pict) {
      formData.append('itemPic', this.pict);
    }

    this.route.paramMap.subscribe((data) => {
      this.itemService
        .updateItem(Number(data.get('id')), formData)
        .subscribe(() => {
          this.router.navigate(['/item']);
        });
    });
  }
}
