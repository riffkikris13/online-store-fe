import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ItemrService } from '../../../service/ItemService';
import { Router } from '@angular/router';

@Component({
  selector: 'item-add',
  templateUrl: './item-add.component.html',
  styleUrl: './item-add.component.scss',
})
export class ItemAddComponent {
  form: FormGroup;
  pict: File | undefined;

  constructor(
    fb: FormBuilder,
    private itemService: ItemrService,
    private router: Router
  ) {
    this.form = fb.group({
      name: ['', Validators.required],
      stock: ['', Validators.required],
      price: ['', Validators.required],
      picture: ['', Validators.required],
    });
  }

  onFileSelected(event: any) {
    this.pict = event.target.files[0];
  }

  onSubmit(): void {
    const formData = new FormData();
    formData.append('itemName', this.form.get('name')?.value);
    formData.append('stock', this.form.get('stock')?.value);
    formData.append('price', this.form.get('price')?.value);
    formData.append('itemPic', this.pict ?? '');

    this.itemService.createItem(formData).subscribe(() => {
      this.router.navigate(['/item']);
    });
  }
}
