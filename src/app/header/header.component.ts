import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss',
})
export class HeaderComponent {
  @Input()
  content: string = 'customer';
  blob: Blob = new Blob();
  @Output()
  currentEvent = new EventEmitter<void>();

  constructor(private router: Router, private http: HttpClient) {}

  onDownload(): void {
    this.http
      .get<any>(`http://localhost:8000/v1/api/order/report`, {
        responseType: 'blob' as 'json',
      })
      .subscribe((data) => {
        this.blob = new Blob([data], { type: 'application/pdf' });

        var downloadURL = window.URL.createObjectURL(data);
        var link = document.createElement('a');
        link.href = downloadURL;
        link.download = 'report.pdf';
        link.click();
      });
  }

  onClick(): void {
    console.log(this.content);

    this.router.navigate(['/' + this.content + '/add']);
  }

  menu(): void {
    this.currentEvent.emit();
  }
}
