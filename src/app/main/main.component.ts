import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrl: './main.component.scss',
})
export class MainComponent {
  @Input()
  currentC: string = 'customer';
  @Output()
  forwardEvent = new EventEmitter<void>();

  receiveContent(): void {
    this.forwardEvent.emit();
  }
}
