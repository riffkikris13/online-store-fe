import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../../../service/CustomerService';
import { Router } from '@angular/router';
import { Customer } from '../../../model/Customer';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'customer-update',
  templateUrl: './customer-update.component.html',
  styleUrl: './customer-update.component.scss',
})
export class CustomerUpdateComponent implements OnInit {
  form: FormGroup;
  pict: File | undefined;
  customer: Customer | null = null;

  constructor(
    fb: FormBuilder,
    private customerService: CustomerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.form = fb.group({
      name: [this.customer?.customerName, Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((data) => {
      this.customerService
        .getCustomer(Number(data.get('id')))
        .subscribe((response) => {
          this.customer = response;
          this.setFormValues(response);
        });
    });
  }

  setFormValues(customer: Customer): void {
    this.form.patchValue({
      name: customer.customerName,
      address: customer.customerAddress,
      phone: customer.customerPhone,
    });
  }

  onFileSelected(event: any) {
    this.pict = event.target.files[0];
  }

  onSubmit(): void {
    const formData = new FormData();
    formData.append('customerName', this.form.get('name')?.value);
    formData.append('customerAddress', this.form.get('address')?.value);
    formData.append('customerPhone', this.form.get('phone')?.value);
    if (this.pict) {
      formData.append('customerPic', this.pict);
    }

    this.route.paramMap.subscribe((data) => {
      this.customerService
        .updateCustomer(Number(data.get('id')), formData)
        .subscribe(() => {
          this.router.navigate(['/customer']);
        });
    });
  }
}
