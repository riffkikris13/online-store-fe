import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../service/CustomerService';
import { ActivatedRoute } from '@angular/router';
import { Customer } from '../../../model/Customer';

@Component({
  selector: 'customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrl: './customer-detail.component.scss',
})
export class CustomerDetailComponent implements OnInit {
  customer: Customer | null = null;

  constructor(
    private customerService: CustomerService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((data) => {
      this.customerService
        .getCustomer(Number(data.get('id')))
        .subscribe((response) => {
          this.customer = response;
        });
    });
  }
}
