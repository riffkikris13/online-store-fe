import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { Customer } from '../../../model/Customer';
import { CustomerService } from '../../../service/CustomerService';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'customer-content',
  templateUrl: './customer-content.component.html',
  styleUrl: './customer-content.component.scss',
})
export class CustomerContentComponent implements AfterViewInit {
  customers: Customer[] = [];
  totalData: number = 0;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  name: string = '';

  constructor(private customerService: CustomerService) {}

  ngAfterViewInit(): void {
    this.customerService
      .getCustomers(
        this.name,
        this.paginator.pageIndex,
        this.paginator.pageSize
      )
      .subscribe((data) => {
        this.customers = data.customers;
        this.totalData = data.totalPages;
      });
  }

  onPageChange() {
    this.customerService
      .getCustomers(
        this.name,
        this.paginator.pageIndex,
        this.paginator.pageSize
      )
      .subscribe((data) => {
        this.customers = data.customers;
        this.totalData = data.totalPages;
      });
  }

  onSubmit(): void {
    this.customerService
      .getCustomers(
        this.name,
        this.paginator.pageIndex,
        this.paginator.pageSize
      )
      .subscribe((data) => {
        this.customers = data.customers;
        this.totalData = data.totalPages;
      });
    this.name = '';
  }

  deleteCustomer(id: number): void {
    this.customerService
      .deleteCustomerAndRefresh(
        id,
        this.paginator.pageIndex,
        this.paginator.pageSize,
        this.name
      )
      .subscribe((data) => {
        this.customers = data.customers;
        this.totalData = data.totalPages;
      });
  }
}
