import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomerService } from '../../../service/CustomerService';

@Component({
  selector: 'customer-add',
  templateUrl: './customer-add.component.html',
  styleUrl: './customer-add.component.scss',
})
export class CustomerAddComponent {
  form: FormGroup;
  pict: File | undefined;

  constructor(
    fb: FormBuilder,
    private customerService: CustomerService,
    private router: Router
  ) {
    this.form = fb.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
    });
  }

  onFileSelected(event: any) {
    this.pict = event.target.files[0];
  }

  onSubmit(): void {
    const formData = new FormData();
    formData.append('customerName', this.form.get('name')?.value);
    formData.append('customerAddress', this.form.get('address')?.value);
    formData.append('customerPhone', this.form.get('phone')?.value);
    if (this.pict) {
      formData.append('customerPic', this.pict);
    }

    this.customerService.createCustomer(formData).subscribe(() => {
      this.router.navigate(['/customer']);
    });
  }
}
