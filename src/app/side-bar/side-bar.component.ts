import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'side-bar',
  templateUrl: './side-bar.component.html',
  styleUrl: './side-bar.component.scss',
})
export class SideBarComponent implements OnChanges {
  @Input()
  currentC: string = '';
  @Output()
  currentEvent = new EventEmitter<string>();
  @Output()
  backEvent = new EventEmitter<boolean>();
  sideBarMenu: number = 0;

  constructor(private router: Router) {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.currentC);

    switch (this.currentC) {
      case 'customer':
        this.sideBarMenu = 1;
        break;
      case 'item':
        this.sideBarMenu = 2;
        break;
      case 'order':
        this.sideBarMenu = 3;
    }
  }

  changeMenu(value: number, path: string) {
    this.sideBarMenu = value;
    this.currentEvent.emit(path);
    this.router.navigate(['/' + path]);
  }

  backEventEmit() {
    this.backEvent.emit(false);
  }
}
