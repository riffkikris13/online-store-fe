import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from './header/header.component';
import { CustomerContentComponent } from './customer/customer-content/customer-content.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { CustomerDetailComponent } from './customer/customer-detail/customer-detail.component';
import { CustomerAddComponent } from './customer/customer-add/customer-add.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { CustomerUpdateComponent } from './customer/customer-update/customer-update.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ItemAddComponent } from './item/item-add/item-add.component';
import { ItemContentComponent } from './item/item-content/item-content.component';
import { ItemDetailComponent } from './item/item-detail/item-detail.component';
import { ItemUpdateComponent } from './item/item-update/item-update.component';
import { OrderContentComponent } from './order/order-content/order-content.component';
import { OrderAddComponent } from './order/order-add/order-add.component';
import { OrderDetailComponent } from './order/order-detail/order-detail.component';
import { OrderUpdateComponent } from './order/order-update/order-update.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { provideNativeDateAdapter } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    MainComponent,
    HeaderComponent,
    CustomerContentComponent,
    CustomerDetailComponent,
    CustomerAddComponent,
    CustomerUpdateComponent,
    ItemAddComponent,
    ItemContentComponent,
    ItemDetailComponent,
    ItemUpdateComponent,
    OrderContentComponent,
    OrderAddComponent,
    OrderDetailComponent,
    OrderUpdateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatIconModule,
    ReactiveFormsModule,
    MatButtonModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/customer', pathMatch: 'full' },
      { path: 'customer', component: CustomerContentComponent },
      { path: 'customer', component: CustomerContentComponent },
      { path: 'customer/add', component: CustomerAddComponent },
      { path: 'customer/detail/:id', component: CustomerDetailComponent },
      { path: 'customer/update/:id', component: CustomerUpdateComponent },
      { path: 'item', component: ItemContentComponent },
      { path: 'item/add', component: ItemAddComponent },
      { path: 'item/detail/:id', component: ItemDetailComponent },
      { path: 'item/update/:id', component: ItemUpdateComponent },
      { path: 'order', component: OrderContentComponent },
      { path: 'order/add', component: OrderAddComponent },
      { path: 'order/detail/:id', component: OrderDetailComponent },
      { path: 'order/update/:id', component: OrderUpdateComponent },
    ]),
    MatPaginatorModule,
    MatDatepickerModule,
    MatAutocompleteModule,
  ],
  providers: [provideNativeDateAdapter()],
  bootstrap: [AppComponent],
})
export class AppModule {}
